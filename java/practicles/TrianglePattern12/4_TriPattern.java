/* c		d
 * C B		D C
 * c b a	d c b
 * 		D C B A		*/

import java.io.*;

class tri4{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1 ; i <= row ; i++){
			
			int ch = 64 + row;

			for(int j = 1; j <= i ; j++){
			
				if(i%2==0){

					System.out.print((char)ch+" ");
				}else{
					
					System.out.print((char)(ch+32)+" ");
				
				}

				ch--;
			}

			System.out.println();
		}
	}
}
