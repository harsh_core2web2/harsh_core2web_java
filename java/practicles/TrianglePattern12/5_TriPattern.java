/* D		D
 * E F		E F
 * G H I	G H I
 * 		J K L M		*/

import java.io.*;

class tri5{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No of rows : ");
		int row = Integer.parseInt(br.readLine());

		char ch = 64;
		ch += row;

		for(int i = 1 ; i <= row ; i++){

			for(int j = 1; j <= i ; j++){
			
				System.out.print(ch++ +" ");
			}

			System.out.println();
		}
	}
}
