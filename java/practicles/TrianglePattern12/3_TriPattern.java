/* C		D
 * C B		D C
 * B C A	D C B
 * 		D C B A		*/

import java.io.*;

class tri3{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1 ; i <= row ; i++){
			
			char ch = 64;
			ch += row;

			for(int j = 1; j <= i ; j++){
			
				System.out.print(ch-- +" ");
			}

			System.out.println();
		}
	}
}
