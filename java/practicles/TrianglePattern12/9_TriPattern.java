/* 4		5
 * 4 a		5 a
 * 4 b 6 	5 b 7
 * 		5 c 7 d		*/

import java.io.*;

class tri9{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1 ; i <= row ; i++){
			
			int num = row + 1;
			int ch = 95 + i;

			for(int j = 1; j <= i ; j++){

				if(j%2 == 0){
			
					System.out.print((char)ch++ +" ");
		
				}else{
					
					System.out.print(num+" ");
				
				}

				num++;
			}

			System.out.println();
		}
	}
}
