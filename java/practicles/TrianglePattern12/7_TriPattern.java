/* 1		1
 * 2 a		2 a
 * 3 b 3 	3 b 3
 * 		4 c 4 d		*/

import java.io.*;

class tri7{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1 ; i <= row ; i++){
			
			int ch = 95 + i ;

			for(int j = 1; j <= i ; j++){

				if(j%2 == 0){
			
					System.out.print((char)ch +" ");
					ch++;
				}else{
					
					System.out.print(i+" ");
				
				}
			}

			System.out.println();
		}
	}
}
