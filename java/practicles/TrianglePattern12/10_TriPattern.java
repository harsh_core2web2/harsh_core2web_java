/* 1		1
 * b c		b c
 * 4 5 6 	4 5 6
 * 		g h i j		*/

import java.io.*;

class tri10{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No of rows : ");
		int row = Integer.parseInt(br.readLine());

		int num  = 1 ;

		for(int i = 1 ; i <= row ; i++){
			
			int ch = 96+num;

			for(int j = 1; j <= i ; j++){

				if(i%2 == 0){
			
					System.out.print((char)ch +" ");
					ch++;
				}else{
					
					System.out.print(num+" ");
				
				}

				num++;
			}

			System.out.println();
		}
	}
}
