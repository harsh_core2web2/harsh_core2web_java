/* 1		1
 * 1 c		1 c
 * 1 e 3 	1 e 3
 * 		1 h 3 j		*/

import java.io.*;

class tri8{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No of rows : ");
		int row = Integer.parseInt(br.readLine());

		char ch ='a';

		for(int i = 1 ; i <= row ; i++){

			for(int j = 1; j <= i ; j++){

				if(j%2 == 0){
			
					System.out.print((char)ch +" ");
				}else{
					
					System.out.print(j+" ");
				
				}

				ch++;
			}

			System.out.println();
		}
	}
}
