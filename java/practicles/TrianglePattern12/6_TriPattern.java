/* 1		1
 * B C		B C
 * 1 2 3	1 2 3
 * 		G H I H J		*/

import java.io.*;

class tri6{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("No of rows : ");
		int row = Integer.parseInt(br.readLine());

		char ch = 'A';

		for(int i = 1 ; i <= row ; i++){

			for(int j = 1; j <= i ; j++){
			
				if(i%2==0){

					System.out.print(ch +" ");
				}else{
					
					System.out.print(j+" ");
				
				}

				ch++;
			}

			System.out.println();
		}
	}
}
