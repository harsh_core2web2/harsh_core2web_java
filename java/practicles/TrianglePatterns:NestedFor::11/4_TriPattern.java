class triangle4{
			/* 3    	4
			 * 3 6		4 8
			 * 3 6 9  	4 8 12
			 * 		4 8 12 16*/

	public static void main(String[] args){
		
		int row = 4;

		for(int i = 1 ; i<= row ; i++){

			for(int j = 1; j<=i ; j++){
			
				System.out.print(row*j +" ");
		
			}

			System.out.println();
		}

	}
}
