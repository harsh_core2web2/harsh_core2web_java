class triangle9{

	public static void main(String[] args){
	
		int row = 4;
	
		for(int i = 1 ; i <= row; i++){
		
			int ch = row + 64;

			for(int j = row ; j >= i ; j-- ){
			
				System.out.print((char)ch+" ");
				ch--;
			}
			
			System.out.println();
		}
	}
}
