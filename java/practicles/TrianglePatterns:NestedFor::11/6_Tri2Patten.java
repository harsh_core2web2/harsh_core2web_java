/*	3 3 3 
 *	2 2 
 *	1		*/

class triangle6{

	public static void main(String[] args){
	
		int row = 4;
		int temp = row;

		for(int i = 1 ; i <= row ; i++){
		
			for(int j = row ; j >= i ; j--){
			
				System.out.print(temp+" ");
			}

			temp--;

			System.out.println();
		}
	}
}
