class triangle5{
			/* 1    	1
			 * 2 4		2 4
			 * 3 6 9  	3 6 9
			 * 		4 8 12 16*/

	public static void main(String[] args){
		
		int row = 4;

		for(int i = 1 ; i<= row ; i++){

			for(int j = 1; j<=i ; j++){
			
				System.out.print(i*j +" ");
		
			}

			System.out.println();
		}

	}
}
