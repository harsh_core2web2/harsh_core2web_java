class triangle10{

	public static void main(String[] args){
	
		int row = 4;
	
		for(int i = 1 ; i <= row; i++){
		
			int ch = i + 64;

			for(int j = row ; j >= i ; j-- ){
			
				if(ch%2==1){

					System.out.print((char)ch+" ");
				}else{
				
					System.out.print(ch+" ");
				
				}

				ch++;
			}
			
			System.out.println();
		}
	}
}
