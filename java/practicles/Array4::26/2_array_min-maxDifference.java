import java.io.*;

class array2{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.print("Enter Array Elements : ");

		for(int i = 0; i < size; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int min = arr[0];
		int max = arr[0];

		for(int i = 1; i < size; i++){
		
			if(arr[i]>max){
			
				max = arr[i];
			}

			if(arr[i]<min){
			
				min = arr[i];
			}

		}

		System.out.println("The Difference between minimum and maximum element is "+(max-min));
	}
}
