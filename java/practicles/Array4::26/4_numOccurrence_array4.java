import java.io.*;

class array4{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		
		
		System.out.print("Enter Array Elements : ");
		for(int i = 0; i < size; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

	
		System.out.print("Enter the number to check : ");
		int num = Integer.parseInt(br.readLine());
		int cnt = 0;

	
		for(int i = 0; i < size; i++){
		
			if(arr[i] == num ){
				cnt ++;
			}
		}


		if(cnt == 2){
			System.out.println(num+" occures 2 times in array ");
		}else if (cnt > 2){
			System.out.println(num+" occures more than 2 times in array ");
		}
	}
}
