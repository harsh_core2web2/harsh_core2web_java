import java.io.*;

class array7{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];

		
		
		System.out.print("Enter Array Elements : ");
		for(int i = 0; i < size; i++){
				
			arr[i] = (char)br.read();
			br.skip(1);
		}
		
		for(int i = 0; i < size; i++){
				
			if(arr[i]>=97 && arr[i]<=122){
				arr[i] -= 32;
			}
		}
		
		System.out.print("Output : ");
		for(int i = 0; i < size; i++){
				
			System.out.print(arr[i]+" ");
		}
		
	}
}
