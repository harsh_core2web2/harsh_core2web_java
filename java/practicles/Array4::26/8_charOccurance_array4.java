import java.io.*;

class array8{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];

		
		
		System.out.print("Enter Array Elements : ");
		for(int i = 0; i < size; i++){
				
			arr[i] = (char)br.read();
			br.skip(1);
		}
		
		System.out.print("Enter a character : ");
		char ch = (char)br.read();
		br.skip(1);

		int cnt = 0;

		for(int i = 0; i < size; i++){
				
			if(arr[i] == ch ){
				cnt++;
			}
		}
		
		System.out.println(ch+" occurs "+cnt+" times in array");
	}
}
