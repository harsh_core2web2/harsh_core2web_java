import java.io.*;

class array1{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.print("Enter Array Elements : ");
		
		int sum = 0;

		for(int i = 0; i < size; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
			sum += arr[i];
		}
		System.out.println("Array elements' average is "+sum/size);
	}
}
