import java.io.*;

class array6{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		
		System.out.print("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];

		
		
		System.out.print("Enter Array Elements : ");
		for(int i = 0; i < size; i++){
		
			arr[i] = (char)br.read();
			br.skip(1);
		}
		
		int cnt = 0;
		int cc = 0;
		for(int i = 0; i < size ; i++){
			
			if( (arr[i]>=65 && arr[i]<=90) || (arr[i]>= 97 && arr[i] <= 122) ){
				
				if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'||arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
					cnt++;
				}else{
					cc++;
				}
			}

		}

		System.out.println("Count of vowels : "+cnt);
		System.out.println("Count of consonants: "+cc);
	}
}
