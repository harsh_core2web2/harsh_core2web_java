                        
import java.util.*;

class D8{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no of rows : ");
		int row = sc.nextInt();

		int space = 0;
		int col = 0;

		for(int i = 1; i < row*2 ; i++){

			int ch = 64+row;

			if(i<=row){
				space = row-i;
				col   = i*2-1;	
				
			}else{
				space = i - row;
				col -= 2;
				//ch--;
			}

			for(int sp = 1; sp <= space ; sp++){
				System.out.print("\t");
				ch--;
			}

			for(int j = 1; j <= col ; j++){
				if(col/2+1 > j){	
					System.out.print((char)ch-- +"\t");
				}else{
					System.out.print((char)ch++ +"\t");
				}
			}
			System.out.println();
		}
	}
}
