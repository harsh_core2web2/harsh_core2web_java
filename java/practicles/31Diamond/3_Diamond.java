import java.util.*;

class D3{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no of rows : ");
		int row = sc.nextInt();

		int space = 0;
		int col = 0;

		for(int i = 1; i < row*2 ; i++){
		
			int temp1 = i ;
			int temp2 = row*2-i;
			int x = temp2;

			if(i<=row){
				space = row-i;
				col   = i*2-1;	
			}else{
				space = i-row;
				col -= 2;
			}

			for(int sp = 1; sp <= space ; sp++){
				System.out.print("\t");
			}

			for(int j = 1; j <= col ; j++){
			
				if(i<=row){
					if(i>j){
						System.out.print(temp1-- +"\t");
					}else{
						System.out.print(temp1++ +"\t");
					}
				}else{
					if(x>j){
						System.out.print(temp2-- +"\t");
					}else{
						System.out.print(temp2++ +"\t");
					}
				
				}
			}
			System.out.println();
		}
	}
}
