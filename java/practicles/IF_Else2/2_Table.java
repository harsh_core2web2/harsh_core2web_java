class num2{

	public static void main(String[] args){
	
		int num = -564;

		if(num == 0 || num <0 ){
		
			System.out.println("Invalid input : "+num+"\nPlease change the number");
		}else{
		
			if(num%13==0){
		
				System.out.println(num+" is in the table of 13");
		
			}else{
			
				System.out.println(num+" is not in the table of 13");
			}

		}
	}
}
