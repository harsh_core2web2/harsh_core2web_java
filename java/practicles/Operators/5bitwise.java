class operator5{

        public static void main(String[] args){

                byte x = 5;  // 0000 0101
                byte y = 3;  // 0000 0011

                System.out.println(" 5 & 3  :  "+(x&y)); // 0001 --> 1
                System.out.println(" 5 | 3  :  "+(x|y)); // 0111 --> 7
                System.out.println(" 5 ^ 3  :  "+(x^y)); // 0110 --> 6
                System.out.println(" 5 << 1 :  "+(x<<1));// 1010 --> 10	
                System.out.println(" 5 >> 1 :  "+(x>>1));// 0010 --> 2
                System.out.println(" 5 >>> 1 :  "+(x>>>1));// 0010 --> 2

        }
}
