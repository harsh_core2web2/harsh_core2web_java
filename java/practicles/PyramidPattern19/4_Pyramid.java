/*			      A
			    B B B
			  C C C C C
			D D D D D D D			*/

import java.io.*;

class pyramid4{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
			
			int ch = 64 + i;

                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i * 2 - 1; j++){

                                System.out.print((char)ch+" ");
                        }
                        System.out.println();
                }

        }
}
