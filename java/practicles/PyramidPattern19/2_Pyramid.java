/*	    1
	  2 3 4
        5 6 7 8 9
			*/
import java.io.*;

class pyramid2{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

		int num = 1;

                for(int i = 1 ; i <= row ; i++){

                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i * 2 - 1; j++){

                                System.out.print(num++ +" ");
                        }
                        System.out.println();
                }

        }
}
