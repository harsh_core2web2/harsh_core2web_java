/*			      4
			    4 3 4
			  4 3 2 3 4
			4 3 2 1 2 3 4			*/

import java.io.*;

class pyramid6{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
		
			int num = row;

                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i * 2 - 1; j++){
				if(i>j){
                        	        System.out.print(num-- +" ");
				}else{
					System.out.print(num++ +" ");
				}
                        }
                        System.out.println();
                }

        }
}
