/*                            4
			    3 3 3
			  2 2 2 2 2
			1 1 1 1 1 1 1			*/

import java.io.*;

class pyramid8{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

		int num = row ;

                for(int i = 1 ; i <= row ; i++){

                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i * 2 - 1; j++){

                                System.out.print(num+" ");
                        }
                        System.out.println();
			num--;
                }

        }
}
