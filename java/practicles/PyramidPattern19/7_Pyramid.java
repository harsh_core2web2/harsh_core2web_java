/*			      1
			    B B B
			  3 3 3 3 3
			D D D D D D D			*/

import java.io.*;

class pyramid7{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){

                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i * 2 - 1; j++){

				if(i%2==0){
                                	System.out.print((char)(i+64)+" ");
				}else{
					System.out.print(i+" ");
				}

			}
                        System.out.println();
                }

        }
}
