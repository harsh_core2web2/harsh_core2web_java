/*			      A
			    a b a
			  A B C B A
			a b c d c b a			*/

import java.io.*;

class pyramid9{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
		
			int num = 1;

                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i * 2 - 1; j++){
				
				if(i%2==0){
					if(i>j){
                        	        	System.out.print((char)(num+96) +" ");
						num++;
					}else{
						System.out.print((char)(num+96) +" ");
						num--;
					}
				}else{
					  if(i>j){
                                                System.out.print((char)(num+64) +" ");
						num++;
                                        }else{
                                                System.out.print((char)(num+64) +" ");
						num--;
                                        }
				}
                        }
                        System.out.println();
                }

        }
}
