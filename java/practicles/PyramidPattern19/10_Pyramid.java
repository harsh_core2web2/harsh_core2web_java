/*			      D
			    C D C
			  B C D C B
			A B C D C B A			*/

import java.io.*;

class pyramid10{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
		
			int ch  = row - i + 1;

                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= 2 * i - 1; j++){
			
				if(i>j){
                        	        System.out.print((char)(ch+64) +" ");
					ch++;
				}else{
					System.out.print((char)(ch+64) +" ");
					ch--;
				}

                        }
                        System.out.println();
                }

        }
}
