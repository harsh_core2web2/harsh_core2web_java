class while7{

	public static void main (String[] args){
	
		int num = 256985;
		int rem = 0;
		int sum = 0;
		System.out.print("Sum of even digits in "+num+"  : ");
		while(num>0){
		
			rem = num%10;
			num /= 10;
			if(rem%2==0){
				sum += rem;
				}
		}
		System.out.print(sum);
	}
}
