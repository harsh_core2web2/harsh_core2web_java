class while5{

	public static void main(String[] args){
	
		int num = 216985;
		int rem = 0;
		int cube = 0;

		while(num>0){
		
			rem = num%10;
			num /=10;
			
			if(rem%2==0){
			
				cube = rem * rem * rem ;
				System.out.print(cube+" ");
			}
		}
	}
}
