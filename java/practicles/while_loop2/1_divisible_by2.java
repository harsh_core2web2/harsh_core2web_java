class while1{

	public static void main(String[] args){
	
		int rem = 0;
		int num = 2569185;
		System.out.print("Digits of a number in "+num+" divisible by 2 are : ");

		while(num>0){
		
			rem = num%10;
			num /= 10;
		       if(rem%2==0){
		       
			       System.out.print(rem+" ");
		       }	
		}
	}
}
