class while3{

	public static void main(String[] args){
	
		int rem = 0;
		int num = 436780521;
		System.out.print("Digits of a number in "+num+"   divisible by 2 or  3 are : ");

		while(num>0){
		
			rem = num%10;
			num /= 10;
		       	if(rem!=0){

				if(rem%2==0 || rem%3==0){
		       
				       System.out.print(rem+" ");
		       		}	
			}
		
		}
	}
}
