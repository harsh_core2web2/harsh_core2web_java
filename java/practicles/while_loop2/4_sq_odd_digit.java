class while4{

	public static void main(String[] args){
	
		int num = 256985;
		int rem = 0;
		int sq = 0;

		while(num>0){
		
			rem = num%10;
			num /=10;
			
			if(rem%2==1){
			
				sq=rem*rem;
				System.out.print(sq+" ");
			}
		}
	}
}
