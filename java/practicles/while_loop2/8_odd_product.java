class while8{

	public static void main (String[] args){
	
		int num = 256985;
		int rem = 0;
		int pro = 1;
		
		while(num>0){
		
			rem = num%10;
			num /= 10;
			if(rem%2==1){
				pro *= rem;
			}
		}
		System.out.println(pro);
	}
}
