class while10{

	public static void main(String[] args){
	
		int num = 9367924;
		int rem = 0;
		int pro = 1;
		int sum = 0;

		while(num>0){
		
			rem = num%10;
			num /=10;
			
			if(rem%2==1){
				sum += rem;
			}else{
			
				pro *=rem;
			}

		}
				System.out.println("Sum of odd digits : "+sum);
				System.out.println("Product of even digits : "+pro);
	}
}
