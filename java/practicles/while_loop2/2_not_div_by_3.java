class while2{

	public static void main(String[] args){
	
		int rem = 0;
		int num = 2569185;
		System.out.print("Digits of a number in "+num+" not  divisible by 3 are : ");

		while(num>0){
		
			rem = num%10;
			num /= 10;
		       if(rem%3!=0){
		       
			       System.out.print(rem+" ");
		       }	
		}
	}
}
