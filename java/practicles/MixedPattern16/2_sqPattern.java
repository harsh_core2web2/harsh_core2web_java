/*	C3 C2 C1	D4 D3 D2 D1
 *	C4 C3 C2	D5 D4 D3 D2
 *	C5 C6 C6	D6 D5 D4 D3
 *			D7 D6 D5 D4	*/

import java.io.*;

class pattern2{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		int ch = 64 + row;

		for(int i = 1; i <= row ; i++){
		
			int num = row + i - 1;

			for(int j = 1; j <= row; j++){
			
				System.out.print((char)(ch));
				System.out.print(num-- +" ");
			}System.out.println();
		}
	}
}
