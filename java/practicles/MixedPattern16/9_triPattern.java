/*	1 2 3 		1 2 3 4 
 *	C B		C B A
 *	1		1 2
 *			A	*/

import java.io.*;

class pattern9{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row ; i++){
			
			int ch = row + 65 - i;

			for(int j = 1; j <= row - i + 1; j++){
				if(i%2==0){
					System.out.print((char)ch-- +" ");		
				}else{
					System.out.print(j+" ");
				}
			}System.out.println();
		}
	}
}
