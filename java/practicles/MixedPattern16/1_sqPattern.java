/*	1  2  3  4  	1 2 3
 *	5  6  7  8  	4 5 6
 *	9  10 11 12	7 8 9		*/

import java.io.*;

class pattern1{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		int num = 1;

		for(int i = 1; i <= row ; i++){
		
			for(int j = 1; j <= row; j++){
			
				System.out.print(num++ +" ");
			}System.out.println();
		}
	}
}
