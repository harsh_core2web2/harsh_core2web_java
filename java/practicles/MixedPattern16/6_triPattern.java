/*	c 		d 
 *	3 2		4  3
 *	c b a		d  c  b 
 *			4  3  2  1	*/

import java.io.*;

class pattern6{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row ; i++){
		
			int ch = row + 96;
			int temp = row;

			for(int j = 1; j <= i; j++){
			
				if(i%2==0){
					
					System.out.print(temp-- +" ");
				}else{
					System.out.print((char)ch-- +" ");	
				}
			}System.out.println();
		}
	}
}
