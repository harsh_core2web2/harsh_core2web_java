/*	F E D 		J I G H 
 *	C B		F E D
 *	A		C B
 *			A	*/

import java.io.*;

class pattern8{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());
		
		int ch = (row*(row+1))/2 + 64;

		for(int i = 1; i <= row ; i++){

			for(int j = 1; j <= row - i + 1; j++){
			
				System.out.print((char)ch-- +" ");		
			
			}System.out.println();
		}
	}
}
