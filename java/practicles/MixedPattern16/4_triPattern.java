/*	3 		4 
 *	2 4		3  6
 *	1 2 3		2  4  6 
 *			1  2  3  4	*/

import java.io.*;

class pattern4{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row ; i++){

			int x = row - i + 1;

			for(int j = 1; j <= i; j++){

				System.out.print(x*j+" ");
					
			}System.out.println();
		}
	}
}
