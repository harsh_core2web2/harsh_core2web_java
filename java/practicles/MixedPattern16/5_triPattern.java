/*	1 		1 
 *	2 4		2  4
 *	3 6 9		3  6  9 
 *			4  8  12  16i	*/

import java.io.*;

class pattern5{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row ; i++){

			for(int j = 1; j <= i; j++){

				System.out.print(i*j+" ");
					
			}System.out.println();
		}
	}
}
