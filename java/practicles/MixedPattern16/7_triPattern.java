/*	2  4  6 	2  4  6  8 
 *	8  10		10 12 14
 *	12		16 18
 *			20	*/

import java.io.*;

class pattern7{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());
		int num = 2;

		for(int i = 1; i <= row ; i++){

			for(int j = 1; j <= i; j++){
			
				System.out.print(num+" ");	
				num += 2;	
			
			}System.out.println();
		}
	}
}
