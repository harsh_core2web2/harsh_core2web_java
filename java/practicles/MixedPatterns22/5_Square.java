/*	 	        	     1
			         4   7
			    10  11  15			*/
import java.io.*;

class pattern5{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Rows = ");
                int row = Integer.parseInt(br.readLine());

		int num = 3;

                for(int i = 1 ; i <= row ; i++){

                        for(int j = 1 ; j <= row; j++){

                                System.out.print(num*num +" ");
				num++;
                        }
                        System.out.println();
                }

        }
}
