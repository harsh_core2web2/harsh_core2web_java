/*	 	        	     1
			         4   7
			    10  11  15			*/
import java.io.*;

class pattern3{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Rows = ");
                int row = Integer.parseInt(br.readLine());

		int num = 1;

                for(int i = 1 ; i <= row ; i++){
			
                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i; j++){

                                System.out.print(num+" ");
				num += 3;
                        }
                        System.out.println();
                }

        }
}
