/*			    3
			  2 3 4
			1 2 3 4 5			*/

import java.io.*;

class pattern8{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
			
			int temp = row - i + 1;

                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i * 2 - 1; j++){

                                System.out.print(temp++ +" ");
                        }
                        System.out.println();
                }

        }
}
