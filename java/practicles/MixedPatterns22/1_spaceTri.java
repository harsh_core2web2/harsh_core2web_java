/*	 	        	     1
			         3   5
			     7   9  11
			13  15  17  19			*/
import java.io.*;

class pattern1{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Rows = ");
                int row = Integer.parseInt(br.readLine());

		int num = 1;

                for(int i = 1 ; i <= row ; i++){
			
                        for(int sp = 1; sp <= row-i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= i; j++){

                                System.out.print(num+" ");
				num += 2;
                        }
                        System.out.println();
                }

        }
}
