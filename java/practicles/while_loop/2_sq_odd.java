class while2{

	public static void main(String[] args){
	
		int start = 150;
		int end = 198;
		int sq = 0;

		for(int i = start ; i <= end; i++){
		
			sq = i*i;
			if(sq%2==1){
			
				System.out.print(i+" ");
			}
		}
	}
}
