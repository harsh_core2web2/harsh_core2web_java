/*	2  4  6  8  	2  4  6
 *	10 12 14  	8  10  
 *	16 18  		12
 *	20  				*/

import java.io.*;

class pattern2{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		int x = 2;

		for(int i=1; i <= row; i++){

			for(int j = 1; j<=row-i+1; j++){
			
				System.out.print(x+" ");
				x += 2;
			}

			System.out.println();
		}
	}
}
