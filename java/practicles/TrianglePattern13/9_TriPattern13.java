/*	19 17 15 13  	11 9 7
 *	11 9  7  	5  3
 *	5  3  		1
 *	1  				*/

import java.io.*;

class pattern9{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		int seq = (row*(row+1))-1;

		for(int i=1; i <= row; i++){

			for(int j = 1; j<=row-i+1; j++){
			
				System.out.print(seq+" ");
				seq -= 2;
			}

			System.out.println();
		}
	}
}
