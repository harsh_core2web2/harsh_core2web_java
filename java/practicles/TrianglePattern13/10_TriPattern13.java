/*	D C B A  	c b a
 *	c b a	  	B A
 *	B A  		a
 *	a  				*/

import java.io.*;

class pattern10{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i <= row; i++){

			int seq = row - i + 1 ;

			for(int j = 1; j<=row-i+1; j++){
				
				if(row%2==0){
				
					if(i%2==0){
					
						System.out.print((char)(seq+96)+" ");
					}else{
						System.out.print((char)(seq+64)+" ");
					}
				}else{
				
					if(i%2!=0){
					
						System.out.print((char)(seq+96)+" ");
					}else{
						System.out.print((char)(seq+64)+" ");
					}
				}
				seq--;

			}

			System.out.println();
		}
	}
}
