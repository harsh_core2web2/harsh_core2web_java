/*	4 3 2 1  	3 2 1
 *	C B A  		B A
 *	2 1  		1
 *	A  				*/

import java.io.*;

class pattern8{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i <= row; i++){

			int num = row - i + 1;

			for(int j = 1; j<=row-i+1; j++){
			
				if(i%2==0){

					System.out.print((char)(num+64)+" ");

				}else{
				
					System.out.print(num+" ");
				}

				num--;

			}

			System.out.println();
		}
	}
}
