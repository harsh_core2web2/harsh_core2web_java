/*	1 2 3 4  	1 2 3
 *	2 3 4  		2 3 
 *	3 4  		3
 *	4  				*/

import java.io.*;

class pattern1{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i <= row; i++){
		
			int x = i;

			for(int j = 1; j<=row-i+1; j++){
			
				System.out.print(x++ +" ");
			}

			System.out.println();
		}
	}
}
