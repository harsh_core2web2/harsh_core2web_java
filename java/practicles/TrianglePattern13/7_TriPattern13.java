/*	4 c 2 a  	3 b 1
 *	3 b 1  		2 a  
 *	2 a  		1
 *	1  				*/

import java.io.*;

class pattern7{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i <= row; i++){

			int num = row - i + 1;

			for(int j = 1; j<=row-i+1; j++){
			
				if(j%2==0){

					System.out.print((char)(num+96)+" ");
				}else{
				
					System.out.print(num+" ");
				}

				num--;

			}

			System.out.println();
		}
	}
}
