/*	20 18 16 14  	12 10 8
 *	12 10 8  	6  4  
 *	6  4  		2
 *	2  				*/

import java.io.*;

class pattern3{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());

		int Tseq = (row*(row+1));

		for(int i=1; i <= row; i++){

			for(int j = 1; j<=row-i+1; j++){
			
				System.out.print(Tseq+" ");
				Tseq -= 2;
			}

			System.out.println();
		}
	}
}
