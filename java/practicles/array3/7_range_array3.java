import java.util.*;
// size odd & >= 5 print odd else even
class array27{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size : ");
                int size = sc.nextInt();

                int arr[] = new int[size] ;

                System.out.print("Enter array elements : ");

                for(int i=0; i < size ; i++){
                        arr[i] = sc.nextInt();
                }
                
		System.out.print("Output : ");

                for(int i=0; i < size ; i++){

                        if(size%2 == 1 && size >= 5){
				if(arr[i]%2 != 0 ){
					System.out.print(arr[i]+" ");
				}
			}else{
				if(arr[i]%2==0){
                       			System.out.print(arr[i]+" ");
		
				}
			}
                }

        }
}
