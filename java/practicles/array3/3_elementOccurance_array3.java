import java.util.*;
// Find first occurance of specific number
class array23{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter array elements : ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
		}
		
		System.out.print("Specific no : ");
		int num = sc.nextInt();
		int cnt = 0;

		for(int i = 0; i < size; i++){
			if(arr[i]==num){
				cnt++;
			}
		}
				
		if(cnt>0){
			System.out.println(num+" occured "+cnt+" times in array");
		}else{
			System.out.println(num+" not found");
		}
	}
}
