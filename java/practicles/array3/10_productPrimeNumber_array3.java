import java.util.*;
// Print product of Prime no
class array30{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter array elements : ");

		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
		}

		int product = 1;
		for(int i = 0; i < size; i++){
			
			int cnt =0;
			if(arr[i]>0){
				for(int j = arr[i]; j>0 ; j--){
					if(arr[i]%j==0){
						cnt++;
					}
				}
			}else{
				for(int j = arr[i]; j <= -1 ; j++){
					if(arr[i]%j==0){
						cnt++;
					}
				}

			}
			if(cnt == 2){
				product*=arr[i];
			}
		}
		
		System.out.print("Product of prime numbers : "+product);

	}
}
