import java.util.*;
// Print Prime no
class array29{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter array elements : ");

		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
		}


		System.out.print("Prime numbers are : ");
		
		for(int i = 0; i < size; i++){
			
			int cnt =0;
			if(arr[i]>0){
				for(int j = arr[i]; j>0 ; j--){
					if(arr[i]%j==0){
						cnt++;
					}
				}
			}else{
				for(int j = arr[i]; j <= -1 ; j++){
					if(arr[i]%j==0){
						cnt++;
					}
				}

			}
			if(cnt == 2){
				System.out.print(arr[i]+" ");
			}
		}

	}
}
