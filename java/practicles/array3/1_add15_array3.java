import java.util.*;
// Add 15 in each elements of an array
class array21{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter array elements : ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
			arr[i]+= 15;
		}
		
		System.out.print("New array : ");
		for(int i = 0; i < size; i++){
			System.out.print(arr[i]+" ");
		}

	}
}
