import java.util.*;
// Convert Even elements to 0 & odd to 1
class array24{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter array elements : ");
		for(int i = 0; i < size; i++){
			arr[i] = sc.nextInt();
			if(arr[i]%2==0){
				arr[i]= 0;
			}else{
				arr[i] = 1;
			}
		}
		
		System.out.print("New array : ");
		for(int i = 0; i < size; i++){
			System.out.print(arr[i]+" ");
		}

	}
}
