import java.util.*;

class array1{
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Size of Array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < size ; i++){
			arr[i] = sc.nextInt();
		}
		
		int flag = 0;
		for(int i = 0 ; i < size-1 ; i++){
			if(arr[i] <= arr[i+1]){
			}else{
				flag = 1;
				break;
			}
		}

		if(flag == 0){
			System.out.println("The given Array is in Ascending Order");
		}else{
			System.out.println("The given Array is not in ascending Order");
		}
	}
}
