// range 100 - 24
// Print nos which are only divisible by 4 & 5

class while10{

	public static void main(String[] args){
	
		int i = 100;

		while(i>=24){
		
			if(i%4==0 && i%5==0){
			
				System.out.print(i+" ");
			}i--;
		}
	}
}
