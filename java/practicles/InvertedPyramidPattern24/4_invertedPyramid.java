/*			1 2 3 4 5 6 7
			  1 2 3 4 5
			    1 2 3
			      1				*/

import java.io.*;

class pyramid4{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
			
			int num = 1;

                        for(int sp = 1; sp < i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = (row - i )*2 + 1; j >= 1; j--){

                                System.out.print(num++ +" ");
                        }
                        System.out.println();
                }

        }
}
