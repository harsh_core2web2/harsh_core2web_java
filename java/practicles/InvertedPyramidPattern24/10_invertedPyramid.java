/*			4 3 2 1 2 3 4
			  3 2 1 2 3
			    2 1 2
			      1				*/
import java.io.*;

class pyramid10{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
		
			int num  = row-i + 1;

                        for(int sp = 1; sp < i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = (row - i )*2 + 1; j >= 1; j--){
	
				int x = (row - i)*2 +1;	

				if(j> x/2 + 1){
                                	System.out.print(num-- +" ");
				}else{
					System.out.print(num++ +" ");
				}

                        }
                        System.out.println();
                }

        }
}
