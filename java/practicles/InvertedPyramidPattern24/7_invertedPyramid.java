/*			A B C D C B A
			  A B C B A
			    A B A
			      A				*/
import java.io.*;

class pyramid7{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
		
			int ch = 65;

                        for(int sp = 1; sp < i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = (row - i )*2 + 1; j >= 1; j--){
	
				int x = (row - i)*2 +1;	

				if(j> x/2 + 1){
                                	System.out.print((char)ch++ +" ");
				}else{
					System.out.print((char)ch-- +" ");
				}

                        }
                        System.out.println();
                }

        }
}
