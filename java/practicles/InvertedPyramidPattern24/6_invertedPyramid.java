/*			D D D D D D D
			  C C C C C
			    B B B
			      A				*/

import java.io.*;

class pyramid6{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){
		
			int ch = row - i + 65;

                        for(int sp = 1; sp < i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = (row - i )*2 + 1; j >= 1; j--){

                                System.out.print((char)ch+" ");
                        }
                        System.out.println();
                }

        }
}
