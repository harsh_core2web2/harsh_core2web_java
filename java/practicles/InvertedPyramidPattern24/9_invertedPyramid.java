/*			1 0 1 0 1 0 1
			  1 0 1 0 1
			    1 0 1
			      1				*/

import java.io.*;

class pyramid9{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){

                        for(int sp = 1; sp < i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = (row - i )*2 + 1; j >= 1; j--){
				
				if(j%2==0){
                                	System.out.print("0 ");
				}else{
					System.out.print("1 ");
				}

                        }
                        System.out.println();
                }

        }
}
