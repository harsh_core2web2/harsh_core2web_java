/*			A A A A A A A
			  B B B B B
			    C C C
			      D			*/

import java.io.*;

class pyramid5{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Size = ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= row ; i++){

                        for(int sp = 1; sp < i; sp++){

                                System.out.print("  ");
                        }

                        for(int j = (row - i )*2 + 1; j >= 1; j--){

                                System.out.print((char)(i+64)+" ");
                        }
                        System.out.println();
                }

        }
}
