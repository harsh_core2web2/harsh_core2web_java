import java.util.*;
// print sum of odd elements 
class array4{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size] ;

		System.out.print("Enter array elements : ");

		for(int i=0; i < size ; i++){
			arr[i] = sc.nextInt();
		}

		int sum = 0;

		for(int i=0; i < size ; i++){
		
			if(arr[i]%2!=0){
				sum += arr[i];
			}
		}

		System.out.println("Sum of odd elements : "+sum);
	}
}
