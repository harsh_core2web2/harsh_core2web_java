import java.util.*;
// print Character Array
class array6{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size : ");
		int size = sc.nextInt();

		char arr[] = new char[size] ;

		System.out.print("Enter array elements : ");

		for(int i=0; i < size ; i++){
			arr[i] = sc.next().charAt(0);
		}
		
		System.out.print("Array : ");

		for(int i=0; i < size ; i++){
		
			System.out.print(arr[i]+" ");
		}

	}
}
