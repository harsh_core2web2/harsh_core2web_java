import java.util.*;

class array2{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size] ;

		System.out.print("Enter array elements : ");

		for(int i=0; i < size ; i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Array : ");

		for(int i=0; i < size ; i++){
		
			System.out.print(arr[i]+" ");
		}
	
	}
}
