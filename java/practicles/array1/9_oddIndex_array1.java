import java.util.*;
// print odd index elements 
class array9{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size] ;

		System.out.print("Enter array elements : ");

		for(int i=0; i < size ; i++){
			arr[i] = sc.nextInt();
		}

		for(int i=0; i < size ; i++){
		
			if(i%2!=0){
				System.out.println(arr[i]+" is an odd indexed element");
			}
		}
	}
}
