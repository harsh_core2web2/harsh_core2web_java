/*	    C		      D
 *	  B C		    C D
 *	A B C		  B C D
 *			A B C D		*/

import java.io.*;

class space3{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows = ");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1 ; i <= row ; i++){

			int ch = 65 + row - i ;
		
			for(int sp = 1; sp <= row-i; sp++){
			
				System.out.print("  ");
			}
			
			for(int j = 1 ; j <= i; j++){
			
				System.out.print((char)ch++ +" ");
			}
			System.out.println();
		}

	}
}
