/*	    A  66  C  	65 B 67  D
 *	       66  C 	   B 67  D
 *	           C	     67  D
 *	        	         D	*/


import java.io.*;

class space10{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows = ");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1; i <= row; i++){
			
			int ch = 64 + i ;

			for(int sp = 1; sp < i; sp++){
			
				System.out.print("  ");
			}
			
			for(int j = 1; j <= row-i+1; j++){
			
				if(row%2==0){

					if(i%2==0){
						
						if(j%2==0){
							System.out.print(ch+" ");
						}else{
							System.out.print((char)ch+" ");
						}
					}else{
						if(j%2==1){
							System.out.print(ch+" ");
						}else{
							System.out.print((char)ch+" ");
						}
					}
				}else{
				
					if(i%2==1){
						
						if(j%2==0){
							System.out.print(ch+" ");
						}else{
							System.out.print((char)ch+" ");
						}
					}else{
						if(j%2==1){
							System.out.print(ch+" ");
						}else{

							System.out.print((char)ch+" ");
						}
					}
					
				}
				ch++;
					
					
			}
			System.out.println();
		}

	}
}
