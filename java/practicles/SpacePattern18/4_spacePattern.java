/*	    3		         4
 *	  3 6		      4  8
 *	3 6 9		   4  8  12
 *			4  8  12 16		*/

import java.io.*;

class space4{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows = ");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1 ; i <= row ; i++){
		
			for(int sp = 1; sp <= row-i; sp++){
			
				System.out.print("  ");
			}
			
			for(int j = 1 ; j <= i; j++){
			
				System.out.print(row*j +" ");
			}
			System.out.println();
		}

	}
}
