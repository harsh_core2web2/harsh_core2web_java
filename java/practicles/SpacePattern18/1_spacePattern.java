/*	    1		      1
 *	  1 2		    1 2
 *	1 2 3		  1 2 3
 *			1 2 3 4		*/

import java.io.*;

class space1{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows = ");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1 ; i <= row ; i++){
		
			for(int sp = 1; sp <= row-i; sp++){
			
				System.out.print("  ");
			}
			
			for(int j = 1 ; j <= i; j++){
			
				System.out.print(j+" ");
			}
			System.out.println();
		}

	}
}
