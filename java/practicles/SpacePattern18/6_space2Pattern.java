/*	    3 3 3  	4  4  4  4
 *	      2 2  	   3  3  3
 *	        1  	      2  2
 *	        		 1	*/


import java.io.*;

class space6{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows = ");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1; i <= row; i++){
		
			int temp = row-i+1;

			for(int sp = 1; sp < i; sp++){
			
				System.out.print("  ");
			}
			
			for(int j = 1; j <= row-i+1; j++){
			
				System.out.print(temp +" ");
			}
			System.out.println();
		}

	}
}
