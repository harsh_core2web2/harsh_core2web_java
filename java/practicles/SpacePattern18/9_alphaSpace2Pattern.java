/*	    C B A  	D C B A
 *	      C B   	  D C B
 *	        C  	    D C
 *	        	      D		*/


import java.io.*;

class space9{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows = ");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1; i <= row; i++){
			
			int ch = row+64 ;

			for(int sp = 1; sp < i; sp++){
			
				System.out.print("  ");
			}
			
			for(int j = 1; j <= row-i+1; j++){
			
				System.out.print((char)ch-- +" ");
			}
			System.out.println();
		}

	}
}
