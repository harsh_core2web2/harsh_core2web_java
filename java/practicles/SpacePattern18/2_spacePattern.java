/*	    3		      4
 *	  3 2		    4 3
 *	3 2 1		  4 3 2
 *			4 3 2 1		*/

import java.io.*;

class space2{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows = ");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1 ; i <= row ; i++){

			int temp = row;
		
			for(int sp = 1; sp <= row-i; sp++){
			
				System.out.print("  ");
			}
			
			for(int j = 1 ; j <= i; j++){
			
				System.out.print(temp-- +" ");
			}
			System.out.println();
		}

	}
}
