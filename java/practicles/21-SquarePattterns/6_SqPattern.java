/*	9   4   25
 *	6   49  8
 *	81  10  121	*/

import java.io.*;

class pattern6{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		int temp = row;

		for(int i = 1 ; i <= row; i++){
		
			for(int j = 1; j <= row ; j++){
			
				if(temp%2!=0){
				
					System.out.print(temp*temp+" ");

				}else{
				
					System.out.print(temp+" ");

				}
				
				temp++;

			}
			
			System.out.println();
		}
	}
}
