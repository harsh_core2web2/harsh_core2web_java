/*	2  6  6		2  6  6  12
 *	3  4  9		3  4  9  8 
 *	2  6  6 	2  6  6  12
 *			3  4  9  8*/

import java.io.*;

class pattern9{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1 ; i <= row; i++){
		
			for(int j = 1; j <= row ; j++){
			
				if(i%2==0){
					if(j%2==0){
						System.out.print(j*2+" ");
					}else{
						System.out.print(j*3+" ");
					}
				}else{
					if(j%2==0){
						System.out.print(j*3+" ");
					}else{
						System.out.print(j*2+" ");
					}
				}
			}
			
			System.out.println();

		}
	}
}
