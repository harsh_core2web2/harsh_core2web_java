/*	3  B  1		4  C  2  A
 *	C  B  A		D  C  B  A
 *	3  B  1		4  C  2  A
 *			D  C  B  A	*/

import java.io.*;

class pattern10{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		int count = 0;

		for(int i = 1 ; i <= row; i++){
		
			int ch = row + 64;
			int temp = row;
			for(int j = 1; j <= row ; j++){
			
				if(i%2==1){
					if(j%2==1){
						System.out.print(temp+" ");
					}else{
						System.out.print((char)ch+" ");
					}
					temp--;
					ch--;

				}else{
				
					System.out.print((char)ch-- +" ");
				}
				
				temp++;

			}
			
			System.out.println();
			count++;
		}
	}
}
