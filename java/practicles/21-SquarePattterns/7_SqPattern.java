/*	A  4   A
 *	6  B   8 
 *	C  10  C 		*/

import java.io.*;

class pattern7{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		int temp = row;
		for(int i = 1 ; i <= row; i++){
		
			char ch = 64;
			ch += i;

			for(int j = 1; j <= row ; j++){
			
				if(temp%2==0){
				
					System.out.print(temp+" ");
				
				}else{
				
					System.out.print(ch+" ");
				}
				
				temp++;

			}
			
			System.out.println();
		}
	}
}
