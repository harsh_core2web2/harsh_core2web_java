/*	#  C  #	
 *	C  #  B
 *	#  C  #
 *			# D # C
			D # C #
			# D # C
			D # C #		*/

import java.io.*;

class pattern8{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		int count = 0;

		for(int i = 1 ; i <= row; i++){
		
			int ch = row + 64;
			int temp = 1 + count;

			for(int j = 1; j <= row ; j++){
			
				if(temp%2==0){
				
					System.out.print((char)ch +" ");
					ch-- ;
				}else{
				
					System.out.print("# ");
				}
				
				temp++;

			}
			
			System.out.println();
			count++;
		}
	}
}
