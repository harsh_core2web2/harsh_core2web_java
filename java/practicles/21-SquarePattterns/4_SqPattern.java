/*	C  4  5
 *	F  7  8 
 *	I  10 11		*/

import java.io.*;

class pattern4{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());
		int temp = row;

		for(int i = 1 ; i <= row; i++){
		
			char ch = 64;
			ch += temp;

			for(int j = 1; j <= row ; j++){
			
				if(temp%row==0){
				
					System.out.print(ch+" ");
				
				}else{
				
					System.out.print(temp +" ");
				}

				temp++;

			}
			
			System.out.println();
		}
	}
}
