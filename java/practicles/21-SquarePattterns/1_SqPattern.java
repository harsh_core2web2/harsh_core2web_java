/*	C B A
 *	3 3 3 
 *	C B A		*/

import java.io.*;

class pattern1{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1 ; i <= row; i++){
		
			char ch = 64;
			ch += row;

			for(int j = 1; j <= row ; j++){
			
				if(i%2==0){
				
					System.out.print(row+" ");
				
				}else{
				
					System.out.print(ch-- +" ");
				}
			}System.out.println();
		}
	}
}
