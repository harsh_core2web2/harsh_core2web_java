import java.util.*;
// Count and print even elements in an array
class array11{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of array : ");
                int size = sc.nextInt();

                int arr[] = new int[size] ;

                System.out.print("Enter array elements : ");

                for(int i=0; i < size ; i++){
                        arr[i] = sc.nextInt();
                }
		
		int cnt = 0;
                System.out.print("Even Numbers are : ");
	
                for(int i=0; i < size ; i++){
			if(arr[i]%2==0){
                        	System.out.print(arr[i]+" ");
                		cnt++;
			}
		}
                
		System.out.println("\nCount of even elements is : "+cnt);

        }
}
