import java.util.*;
// If user given size is even print alternate elements else print whole array
class array17{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of array : ");
                int size = sc.nextInt();

                int arr[] = new int[size] ;

                System.out.print("Enter array elements : ");

                for(int i=0; i < size ; i++){
                        arr[i] = sc.nextInt();
                }

		System.out.print("Array elements are : ");

                for(int i=0; i < size ; i++){
			if(size%2==0){
				if(i%2==0){
                			System.out.print(arr[i]+" ");
				}
			}else{
				System.out.print(arr[i]+" ");
			}
		}

        }
}
