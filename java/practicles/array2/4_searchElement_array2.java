import java.util.*;
// Search element in array and print its index no
class array14{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of array : ");
                int size = sc.nextInt();

                int arr[] = new int[size] ;

                System.out.print("Enter array elements : ");

                for(int i=0; i < size ; i++){
                        arr[i] = sc.nextInt();
                }
		
                System.out.print("Enter number to search in array : ");
		int num = sc.nextInt();
	
                for(int i=0; i < size ; i++){
			if(arr[i]==num){
                        	System.out.println(arr[i]+" found at index "+i);
			}
		}

        }
}
