import java.util.*;
// Sum of elements divisible by 3
class array12{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of array : ");
                int size = sc.nextInt();

                int arr[] = new int[size] ;

                System.out.print("Enter array elements : ");

                for(int i=0; i < size ; i++){
                        arr[i] = sc.nextInt();
                }

		int sum = 0;
                System.out.print("Elements Divisible by 3 : ");
	
                for(int i=0; i < size ; i++){
			if(arr[i]%3==0){
                        	System.out.print(arr[i]+" ");
                		sum+=arr[i];
			}
		}
                
		System.out.println("\nCount of elements divisible by 3 : "+sum);

        }
}
