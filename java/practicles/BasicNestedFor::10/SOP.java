class pattern{

	public static void main(String[] args){
	
		int x = 10;
		int y = 20;

		char ch1 = 'A';
		char ch2 = 99;

		System.out.println(x+y+" ");
		System.out.println(""+x+y);
		System.out.println(""+x+y+30);
		System.out.println(""+x+y+30+40);
		System.out.println(""+x+y+30+40+""+x+20);//102030401020

		System.out.println("\n\n\n\n");
		System.out.println(ch1);//A
		System.out.println(ch2);//c
		System.out.println(""+ch1+ch2);//Ac
		System.out.println(x+ch1);
		System.out.println(ch2+x);
		System.out.println(x+ch1+""+x+ch1);
		System.out.println(ch1+ch2+10+" "+ch1+ch2+10);//174 Ac10
		System.out.println(30+20+"Shashi"+30+20);
		System.out.println();
	}
}
