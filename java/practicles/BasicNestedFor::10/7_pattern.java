class pattern7{ 

       /*d c b a
       * d c b a
       * d c b a*/	

	public static void main(String[] args){
	
		int row  = 4;

		for(int i = 1; i <= row; i++){
	
			char ch = 'a';	
			ch += row - 1;

			for(int j = 1; j <= row; j++){
		
				System.out.print(ch-- +" ");

			}
		
			System.out.println();
		}


	}
}
