class pattern8{ 
		/*	c1 c2 c3	D1 D2 D3 D4
			c4 c5 c6	D5 D6 D7 D8
			c7 c8 c9	D9 D10 D11 D12
					D13 D14 D15 D16		*/

	public static void main(String[] args){
	
		int row = 4;
		char ch = 64;
		ch += row;
		int num = 1;

		for(int i = 1; i <= row; i++){
			for(int j = 1; j <= row; j++){
			
				System.out.print(ch+""+num++ +" ");
			}

			System.out.println();
		}

	}
}
