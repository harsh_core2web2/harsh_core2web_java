class IC3{
// Range of integer cache is -128 to +127 
	public static void main(String[] args){
	
		int x = 100;
		int y = 110;
		int z = 100;
		
		int a = 128;
		int b = 128;
		System.out.println(System.identityHashCode(x));//1000
		System.out.println(System.identityHashCode(y));//2000
		System.out.println(System.identityHashCode(z));//1000

		// a & b will get memory in heap section
	
		System.out.println(System.identityHashCode(a));//3000
		System.out.println(System.identityHashCode(b));//4000


	}
}
