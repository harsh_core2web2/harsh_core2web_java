class IC2{

	public static void main(String[] args){
	
		int x = 10;
		int y = 10;
		int z = 10;

		int a = 20;
		int b = 20;

		System.out.println(System.identityHashCode(x));//1000
		System.out.println(System.identityHashCode(y));//1000
		System.out.println(System.identityHashCode(z));//1000
		
		System.out.println(System.identityHashCode(a));//2000
		System.out.println(System.identityHashCode(b));//2000
	}
}
