class IC4{

	// Integer Cache stores only INTEGERS not character or their ASCII !!!
	public static void main(String[] args){
	
		char ch1 = 'A';// 65
		char ch2 = 'a';// 97
		char ch3 = 'C';// 66
		
		int x = 65;
		int y = 97;
		int z = 66;

		// only x, y & z are stored in Integer Cache

		System.out.println(System.identityHashCode(ch1));
		System.out.println(System.identityHashCode(x)+"\n");
		
		System.out.println(System.identityHashCode(ch2));
		System.out.println(System.identityHashCode(y)+"\n");
		
		System.out.println(System.identityHashCode(ch3));
		System.out.println(System.identityHashCode(z));


	}
}
