import java.io.*;

class arr42{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Array Size : ");

		int arrSize = Integer.parseInt(br.readLine());

		int arr[] = new int[arrSize];

		for(int i = 0 ; i < arr.length ; i++){
		
			arr[i] = Integer.parseInt(br.readLine());

		}
		
		System.out.print("Array : ");
		
		for(int i = 0 ; i < arr.length ; i++){
		
			System.out.print(arr[i]+" ");
		}
	}
}
