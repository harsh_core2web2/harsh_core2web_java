class arr19{

	public static void main(String[] args){
	
		int empId[] = new int[3]{110,120,130};
// err : array creation with both dimension expression and initalization is illegal
		System.out.println(empId[0]);	
		System.out.println(empId[1]);	
		System.out.println(empId[2]);	
	}
}
