class arr12{

	public static void main(String[] args){

		double [] empId = new double[3];

		System.out.println(empId[0]); // 0.0  0.0  0.0 as default double value in array is 0.0
		System.out.println(empId[1]);
		System.out.println(empId[2]);

		empId[0] = 10;
		empId[1] = 20;
		empId[2] = 30;

		System.out.println(empId[0]); // 10.0
		System.out.println(empId[1]); // 20.0
		System.out.println(empId[2]); // 30.0


	}
}
