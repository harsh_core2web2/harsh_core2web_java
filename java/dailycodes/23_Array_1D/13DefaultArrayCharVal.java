class arr13{

	public static void main(String[] args){

		char [] empId = new char[3];

		System.out.println(empId[0]); // 0.0  0.0  0.0 as default double value in array is 0.0
		System.out.println(empId[1]);
		System.out.println(empId[2]);

		// Think : if empId[0] = 10; lly 20 & 30 SOP gives empty space why ???
		empId[0] = 'A';
		empId[1] = 'B';
		empId[2] = 'C';

		System.out.println(empId[0]); // 10.0
		System.out.println(empId[1]); // 20.0
		System.out.println(empId[2]); // 30.0


	}
}
