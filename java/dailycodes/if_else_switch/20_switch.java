class switch20{

        public static void main(String[] args){

                String friends = "Kanha";

                System.out.println("Before Switch");

                switch(friends){

                        case "Ashish" :

                                System.out.println("Barclays");
                                break;

                        case "Kanha" :

                                System.out.println("BMC Software");
                                break;

                        case "Rahul" :

                                System.out.println("Infosys");
                                break;

                        case "Badhe" :
                                System.out.println("IBM");

                        default :

                                System.out.println("In Default State");
                }

                System.out.println("After Switch");
        }
}
