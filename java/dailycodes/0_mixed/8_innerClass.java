class Outer {

	int x = 10;

	static int y = 20;

	Outer(){
	
		System.out.println("Outer - Counstructor");
	}

	static class Inner{

		static int z = 220;
		
		//Inner obj3 = new Inner();
	
		Inner(){	

			System.out.println("Inner - Constructor");
		}
		
		static void fun(){
			Inner obj3 = new Inner();
			System.out.println("In fun");
		}	
		
		void gun(){
			//Inner obj3 = new Inner();
			System.out.println("In gun");
		}	
	}
} 
class Client {
	
	public static void main(String[] args){
	
		Outer Outobj = new Outer();
		System.out.println(Outer.Inner.z);
		System.out.println(Outobj.x);

		Outer.Inner.fun();

	//	h.fun();
	//	h.gun();

		Outer.Inner obj3 = Outobj.new Inner();
	}
}
