// Constructor is called implicitly

class Demo{

	Demo(int x){
	
		System.out.println("In Constructor");

	}
}

class constDemo{

	public static void main(String[] args){
	
		System.out.println("Start Main");
		Demo obj = new Demo(002);
		System.out.println("End Main");
		
	}
}
