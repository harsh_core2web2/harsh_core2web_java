// this pointer is used to access global varible

class Demo{

	//Global Varibles
	int x = 10;
	int y = 20;

	Demo(int x, int y){	// Local Varible -> scope : constructor
		
		System.out.println("In Constructor");
		System.out.println(x);//30
		System.out.println(y);//40

	}
	
	void fun(int x,int y){
	
		System.out.println(x);//128
		System.out.println(y);//256
		System.out.println(this.x);//10
		System.out.println(this.y);//20
		this.x = x;
		this.y = y;

	}
	
	void printData(){
	
		System.out.println(x);//128
		System.out.println(y);//256
	}
	
}
class constDemo{

	public static void main(String[] args){
	
		Demo obj = new Demo(30,40);
		obj.fun(128,256);
		obj.printData();
	}
}
