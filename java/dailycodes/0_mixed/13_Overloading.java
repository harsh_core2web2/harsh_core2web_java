class Demo{

	void fun(int x,int y){
		System.out.println("int - int");	
	}

	void fun(float x,int y){
		System.out.println("float - int");	
	}

	void fun(int x , float y){
		System.out.println("int - float");	
	}

	public static void main(String[] args){
	
		Demo obj = new Demo();
		obj.fun(10,10);
		obj.fun('A',10);
		obj.fun(10,'A');
	}
}
