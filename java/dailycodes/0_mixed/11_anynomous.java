// Parameterised Anynomous Inner Class

class Demo{

	void fun(Object x){
	
		System.out.println("In fun Demo");
	}
}

class Outer{

	public static void main(String[] args){
	
		Demo obj = new Demo();
		obj.fun(10);
		obj.fun(9.96);
		obj.fun("Shashi");

		
	}
}
