class Demo{
	
	Demo(){
		System.out.println("Demo-Constructor");
	}
	void fun(){
	
		System.out.println("In fun - Outer");
	}
}

class Outer{

	public static void main(String[] args){
	
		new Demo(){
		
			void fun(){
			
				System.out.println("In fun Outer$1");
				System.out.println(new String("sdvf"));
			}
		}.fun();

		Demo obj2 = new Demo(){};

		new Demo();
	}
}
