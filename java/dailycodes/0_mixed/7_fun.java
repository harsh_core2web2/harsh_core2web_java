class Demo{

	static int x = 10;

	public static void main(String[] args){
	
		int x = 20;
		System.out.println(x+" x");
		System.out.println(this.x+" this.x"); // error : non-static variable this cannot be referenced from a static context
	}
}
