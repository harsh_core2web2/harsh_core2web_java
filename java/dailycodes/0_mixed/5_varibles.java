// Global vs Local Varibles

class Demo{

	//Global Varibles
	int x = 10;
	int y = 20;

	Demo(int x, int y){	// Local Varible -> scope : constructor
		
		System.out.println("In Constructor");
		System.out.println(x);//30
		System.out.println(y);//40
	}

	void printData(){
	
		System.out.println(x);//10
		System.out.println(y);//20
	}
}

class constDemo{

	public static void main(String[] args){
	
		Demo obj = new Demo(30,40);
		obj.printData();
	}
}
