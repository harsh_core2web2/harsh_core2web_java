class ThreadDemo extends Thread{

	ThreadDemo(ThreadGroup tg,String str){
		super(tg,str);
	}

	public void run(){
	
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(2000);
		}catch(Exception e){
		
		}
	}
}

class Client{

	public static void main(String[] args){
		
		System.out.println(Thread.currentThread());
		
		ThreadGroup tg = new ThreadGroup("Instagram");
		
		ThreadGroup subTg1 = new ThreadGroup(tg,"Reels");
		ThreadGroup subTg2 = new ThreadGroup(tg,"Post");

		ThreadDemo t1 = new ThreadDemo(subTg1,"Harsh");
		ThreadDemo t2 = new ThreadDemo(subTg1,"Saurabh");
		ThreadDemo t3 = new ThreadDemo(subTg2,"Sanket");
		ThreadDemo t4 = new ThreadDemo(subTg2,"Jiten");

		t1.start();
		t2.start();
		t3.start();
		t4.start();

		System.out.println(tg.getParent().getName());
		System.out.println(tg.activeCount());
		System.out.println(subTg2.activeCount());
		System.out.println(tg.activeGroupCount());
		System.out.println(subTg2.getParent().getName());

	}
}
