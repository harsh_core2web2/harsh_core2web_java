//  Access Specifier - Private
class DemoOne{

	private int x = 10;
	
	int y =20;

	private void fun(){
	
		System.out.println("In fun");
	}
	
	void run(){
	
		System.out.println("In run");
	}

	// Private methods and varibles are accessible only within same class scope
}

class DemoTwo{

	public static void main(String[] args){

		DemoOne obj = new DemoOne();

		System.out.println(obj.x); // error
		
		System.out.println(obj.y);
		
		obj.fun(); // error
		
		obj.run();
	}
}
