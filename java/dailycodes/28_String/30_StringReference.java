class String30{

	public static void main(String[] args){
	
		String str1 = "Badhe";

		String str2 = "Rahul";

		String str3 = new String("BadheRahul");

		String str4 = str3;

		System.out.println(str1); // Badhe
		System.out.println(System.identityHashCode(str1)+"\n"); //1000 SCP

		System.out.println(str2); // Rahul
		System.out.println(System.identityHashCode(str2)+"\n"); //2000 SCP

		System.out.println(str3);// BadheRahul
		System.out.println(System.identityHashCode(str3)+"\n"); //3000 Heap

		System.out.println(str4);// BadheRahul
		System.out.println(System.identityHashCode(str4)); //3000 Point to Heap (BadheRahul)

	}
}
