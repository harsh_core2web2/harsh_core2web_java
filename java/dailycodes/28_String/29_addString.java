class String29{


	public static void main(String[] args){
	
		String str1 = "Badhe";
		String str2 = "Rahul";
		String str3 = str1 + str2;
		String str4 = new String("BadheRahul");

		System.out.println(str1);
		System.out.println(System.identityHashCode(str1)+"\n");//1000 SCP
		
		System.out.println(str2);
		System.out.println(System.identityHashCode(str2)+"\n");//2000 SCP
		
		System.out.println(str3);
		System.out.println(System.identityHashCode(str3)+"\n");//3000 Heap
		
		System.out.println(str4);
		System.out.println(System.identityHashCode(str4)+"\n");//4000 Heap
	}
}
