class String27{

//	String Literal create object on SCP(Method Area) --> Method Area  && new String create object on Heap
	// str 4 create object on heap
	public static void main(String[] args){
	
		String str1 = "Kanha";
		String str2 = "Ashish";
		String str3 = "KanhaAshish";
		String str4 = str1 + str2;

		System.out.println(str1);
		System.out.println(System.identityHashCode(str1)+"\n");//1000
		
		System.out.println(str2);
		System.out.println(System.identityHashCode(str2)+"\n");//2000
		
		System.out.println(str3);
		System.out.println(System.identityHashCode(str3)+"\n");//3000
		
		System.out.println(str4);
		System.out.println(System.identityHashCode(str4)+"\n");//4000
	}
}
