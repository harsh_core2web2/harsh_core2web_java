class String31{

	public static void main(String[] args){
	
		String str1 = "Rohit";
		String str2 = str1;

		String str3 = new String("Virat");
		String str4 = str3;

		String str5 = new String(str1); // Take Rohit(literal) from str1(location) and store it in new address 

		System.out.println(System.identityHashCode(str1));// 1000
		System.out.println(System.identityHashCode(str2));// 1000
		System.out.println(System.identityHashCode(str3));// 2000
		System.out.println(System.identityHashCode(str4));// 2000
		System.out.println(System.identityHashCode(str5));// 3000
	}
}
