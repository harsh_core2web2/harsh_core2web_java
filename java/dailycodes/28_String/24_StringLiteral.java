class String24{

	public static void main(String[] args){
	
		String str1 = "Shashi";
		String str2 = "Core2Web";
		String str3 = "Shashi";

		System.out.println(str1);
		System.out.println(System.identityHashCode(str1));
		System.out.println(str2);
		System.out.println(System.identityHashCode(str2));
		System.out.println(str3);
		System.out.println(System.identityHashCode(str3));
	}
}
