class String25{

	public static void main(String[] args){
	
		String str1 = new String("Shashi");
		String str2 = new String("C2W");
		String str3 = new String("Shashi");

		System.out.println(str1);
		System.out.println(System.identityHashCode(str1));
		System.out.println(str2);
		System.out.println(System.identityHashCode(str2));
		System.out.println(str3);
		System.out.println(System.identityHashCode(str3));
	}
}
