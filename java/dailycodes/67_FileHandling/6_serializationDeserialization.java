import java.io.*;

class Employee implements Serializable{

	int empId;
	String empName;

	Employee(int empId,String empName){
		
		this.empId = empId;
		this.empName = empName;
	}
}

class SerialDemo{

	public static void main(String[] args)throws IOException{
	
		FileOutputStream fos = new FileOutputStream("EmployeeData.txt");

		ObjectOutputStream oos = new ObjectOutputStream(fos);

		Employee obj1 = new Employee(1,"Ramesh");
		Employee obj2 = new Employee(2,"Suresh");

		oos.writeObject(obj1);
		oos.writeObject(obj2);
	}
}

class DeserialDemo{

	public static void main(String[] args)throws Exception{
	
		FileInputStream fis = new FileInputStream("EmployeeData.txt");

		ObjectInputStream ois = new ObjectInputStream(fis);

		Employee obj11 = (Employee)ois.readObject();
		Employee obj22 = (Employee)ois.readObject();

		System.out.println(obj11.empId);
		System.out.println(obj11.empName);
		
		System.out.println(obj22.empId);
		System.out.println(obj22.empName);

	}
}
