import java.io.*;

class DirList{

	public static void main(String[] args)throws IOException{
	
		
		int fileNo = 0;
		File fObj = new File("./../67_FileHandling");
		String names[] = fObj.list();

		
		System.out.println("All files / folders : ");

	
		for(int i = 0; i<names.length;i++){
		
			File x = new File(names[i]);
			System.out.println(x);
		
			if(x.isFile() == true)
				fileNo++;
		}

		System.out.println("Total no of Files : "+fileNo);
		System.out.println("Total no of Folders : "+(names.length-fileNo));
	}

}
