import java.io.*;

class FileReading{

	public static void main(String[] args)throws IOException{
	
		FileReader fr = new FileReader("Incubators.txt");
		
		int ch;

		while((ch = fr.read()) != -1){
		
			System.out.print((char)ch);
		}
		fr.close();
	}
}
