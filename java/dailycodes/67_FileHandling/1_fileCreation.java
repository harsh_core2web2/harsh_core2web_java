import java.io.*;

class FileDemo{

	public static void main(String[] args)throws IOException{
	
		File fObj = new File("Incubators.txt");
		fObj.createNewFile();

		File dirObj = new File("Java2024");
		dirObj.mkdir();

		File fObj2 = new File(dirObj,"Core2Web.txt");
		fObj2.createNewFile();
	}
}
