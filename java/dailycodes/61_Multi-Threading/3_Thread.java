class ThreadDemo extends Thread{

	public void run(){	// run is public in class Thread 
	
		System.out.println(Thread.currentThread().getName());

		for(int i = 01 ; i<= 5 ; i++){
		
			System.out.println("In run");
		}
	}
	
	public static void main(String[] args)throws InterruptedException{

		System.out.println(Thread.currentThread().getName());

		ThreadDemo td = new ThreadDemo();
		td.start();

		Thread.sleep(2000);
		
		for(int i = 01 ; i<= 5 ; i++){
		
			System.out.println("In main");
		}
	}
}
