class ThreadDemo extends Thread{

	public void run(){
	
		System.out.println(Thread.currentThread().getName());//Thread-0

	}

	public static void main(String[] args){
	
		System.out.println(Thread.currentThread().getName());//main
		
		ThreadDemo td = new ThreadDemo();
		td.start();
		
		System.out.println(Thread.currentThread().getName());//main
	}
}
