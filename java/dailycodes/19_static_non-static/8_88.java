// Access fun() gun() & run() without creating object
// error : non - static method ..... cannot be referened from static context
class io7{

	void fun(){
	
		System.out.println("In fun function");
	}

	void run(){
		System.out.println("In run function");
	}

	void gun(){
	
		System.out.println("In gun method");
	}

	public static void main(String[] args){
	
		System.out.println("In main method");
		run();
		gun();
		fun();
	}
}
