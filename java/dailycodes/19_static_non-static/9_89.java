// Access fun() gun() & run() by creating object
class io9{

	void fun(){
	
		System.out.println("In fun function");
	}

	void run(){
		System.out.println("In run function");
	}

	void gun(){
	
		System.out.println("In gun method");
	}

	public static void main(String[] args){
	
		System.out.println("In main method");
		
		io9 obj = new io9();
		
		obj.run();
		obj.gun();
		obj.fun();
	}
}
