// Access fun() & run() by creating object for fun
class io7{

	void fun(){
	
		System.out.println("In fun function");
	}

	static void run(){
		System.out.println("In run function");
	}

	public static void main(String[] args){
	
		System.out.println("In main method");
		run();
		io7 obj = new io7();
		obj.fun();
	}
}
