// error : non-static method fun() cannot be referenced from static context
// Access fun() & run()
class io6{

	void fun(){
	
		System.out.println("In fun function");
	}

	static void run(){
		System.out.println("In run function");
	}

	public static void main(String[] args){
	
		System.out.println("In main method");
		fun();
		run();
	}
}
