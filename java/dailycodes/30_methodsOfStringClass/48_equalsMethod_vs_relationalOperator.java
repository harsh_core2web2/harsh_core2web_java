// Prototype : public boolean equals(Object anObject)
// Boolean indicates value will be true or false
// Relational Operators : == , != 
// equals vs ==

class String48{

	public static void main(String[] args){
	
		String str1 = "Rahul";
		String str2 = "Rahul";

		System.out.println(str1.equals(str2));// content 
		System.out.println(str1==str2);// address

		
		
		String str3 = "Ashish";
		String str4 = new String("Ashish");
		
		System.out.println(str1.equals(str2));// content 
		System.out.println(str3==str4);// address

	}
}
