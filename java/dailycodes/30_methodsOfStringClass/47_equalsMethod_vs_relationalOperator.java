// Prototype : public boolean equals(Object anObject)
// Boolean indicates value will be true or false
// Relational Operators : == , != 
// equals vs ==

class String47{

	public static void main(String[] args){
	
		String str1 = "Rahul";
		String str2 = "Rahul";

		System.out.println(str1.equals(str2)); // compares content
		System.out.println(str1==str2); // compares address
	}
}
