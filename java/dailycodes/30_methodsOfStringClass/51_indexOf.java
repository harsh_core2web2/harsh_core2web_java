// Prototype : public int indexOf(char ch, int fromIndex);

class String51{

	public static void main(String[] args){
	
		String str = "Shashi";
		System.out.println(str.indexOf('n')); // -1 : because n is not preset in string and -1 is the first invalid index
	}
}
