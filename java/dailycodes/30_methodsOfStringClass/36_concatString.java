// Prototype : String concat(String str);

class String36{

	public static void main(String[] args){
	
		String str1 = "Shashi";
		String str2 = "Bagal";

		System.out.println(str1); // Shashi
		System.out.println(str2); // Bagal

		str1.concat(str2);// ShashiBagal; logical err --> strore in str1 or print in SOP

		System.out.println(str1);//Shashi
		System.out.println(str2);//Bagal

	}
}
