import java.util.*;

class st11{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Your name ");
		String name = sc.next();

		System.out.print("Enter society name ");
		String socName = sc.next();

		System.out.print("Enter wing name ");
		char wing = sc.next().charAt(0);

		System.out.print("Enter flatno name ");
		int flatNo = sc.nextInt();
	
		System.out.println("Name : "+name);
		System.out.println("Society : "+socName);
		System.out.println("Wing : "+wing);
		System.out.println("Flat No : "+flatNo);
	}
}
