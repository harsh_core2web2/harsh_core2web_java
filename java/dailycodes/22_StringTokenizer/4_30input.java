import java.io.*;

class st4{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Your Name : ");
		String name = br.readLine();

		System.out.println("Enter Society Name : ");
		String socName = br.readLine();

		System.out.println("Enter Wing : ");
		char wing = br.readLine();// err : String cannot be converted to string

		System.out.println("Enter flatNo : ");
		int flatNo = br.readLine(); // err : String cannot be converted to int {flatno --> int ; br.readline --> takes String input }

	}
}
