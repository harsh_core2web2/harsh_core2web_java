import java.io.*;

class st10{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Your name ");
		String name = br.readLine();

		System.out.print("Enter society name ");
		String socName = br.readLine();

		System.out.print("Enter wing name ");
		char wing =(char)br.read();
	
		br.skip(1);

		System.out.print("Enter flatno name ");
		int flatNo = Integer.parseInt(br.readLine());
	
		System.out.println("Name : "+name);
		System.out.println("Society : "+socName);
		System.out.println("Wing : "+wing);
		System.out.println("Flat No : "+flatNo);
	}
}
