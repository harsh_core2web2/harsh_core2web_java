import java.util.*;

class arr28{

	public static void main(String[] args){
	
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter Size of an Array : ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		for (int i = 0 ; i<arr.length ; i++){
		
			System.out.print("Enter Element: ");
		
			arr[i] = sc.nextInt();
		}

		System.out.print("\nArray : [ ");
		for (int j = 0 ; j< arr.length ; j++){
		
			System.out.print(arr[j]+" ");
		}
		System.out.print(" ]");
	}
}
