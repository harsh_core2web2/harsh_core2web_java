import java.util.*;

class arr29{

	public static void main(String[] args){
	
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter Size of an Array : ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		for (int i = 0 ; i<arr.length ; i++){
		
			System.out.print("Enter Element: ");
		
			arr[i] = sc.nextInt();
		}

		System.out.print("Array :  ");
	
		for (int j = 0 ; j< arr.length ; j++){
		
			System.out.print(arr[j]+" ");
		}

		int sum = 0;

		for (int i = 0 ; i < arr.length ; i++){
		
			sum += arr[i];
		}
		System.out.println("\nSum : "+sum);
	}
}
