class arr24{

	public static void main(String[] args){
	
		int arr[] = new int[]{};

		System.out.println(arr[0]); 
		
		// err : ArrayIndexOutOfBound
		// This error occurs because array is created ; it's of a size zero and we are trying to aceess element out of array limit
	}
}
