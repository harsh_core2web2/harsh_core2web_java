import java.util.*;

class arr27{

	public static void main(String[] args){
	
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter Size of an Array : ");

		int size = sc.nextInt();

		int arr[] = new int[size];
		
		System.out.println("Enter Array Elements: ");

		for (int i = 0 ; i<arr.length ; i++){
		
			arr[i] = sc.nextInt();
		}

		System.out.print("\nArray : [ ");
		for (int j = 0 ; j< arr.length ; j++){
		
			System.out.print(arr[j]+" ");
		}
		System.out.print(" ]");
	}
}
