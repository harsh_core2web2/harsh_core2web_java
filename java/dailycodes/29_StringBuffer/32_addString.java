class String32{

// Strings Are Immutable

	public static void main(String[] args){
	
		String str = new String("Shashi");
		System.out.println(str);
		System.out.println(System.identityHashCode(str)+"\n");

		str = str + "Bagal" ; 
		System.out.println(str);
		System.out.println(System.identityHashCode(str));

	}
}
