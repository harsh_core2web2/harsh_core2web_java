class StringBuffer3{

	// String Buffer Has Capacity of 16 characters 
	
	public static void main(String[] args){
	
		StringBuffer sb = new StringBuffer("Shashi");
		
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity()); //22

		sb = sb + "Bagal"; // error : String cannot be converted to StringBuffer

		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity()); //22
	}
}
