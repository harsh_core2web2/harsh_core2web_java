class String33{

	public static void main(String[] args){
	
		String str = new String("Shashi"); // Object 1
		System.out.println(str); //Shashi

		str = str + "Bagal"; // Object 2
		System.out.println(str); //ShashiBagal
		
		str += "c2w"; // Object 3
		System.out.println(str); //ShashiBagalc2w

	}
}
