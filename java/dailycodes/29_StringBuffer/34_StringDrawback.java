class String34{

	public static void main(String[] args){
	
		String str = new String("Shashi"); // Object 1
		System.out.println(str); //Shashi
		System.out.println(System.identityHashCode(str)); //1000

		str = str + "Bagal"; // Object 2
		System.out.println(str); //ShashiBagal
		System.out.println(System.identityHashCode(str)); //2000
		
		str += "c2w"; // Object 3
		System.out.println(str); //ShashiBagalc2w
		System.out.println(System.identityHashCode(str)); //3000

		// With every change in string a new object is created and this is time consuming process for jvm (TimeComplexity++)
	}
}
