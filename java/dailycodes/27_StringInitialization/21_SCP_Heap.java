class String21{

	public static void main(String[] args){
	
		String str1= "Core2Web"; //String Literal --> SCP
		System.out.println(System.identityHashCode(str1));//1000

		String str2= "Core2Web"; //String Literal --> SCP
		System.out.println(System.identityHashCode(str2));//1000
		
		String str3= new String("Core2Web"); //Object type String or new String --> Heap
		System.out.println(System.identityHashCode(str3));//2000
		
		String str4= new String("Core2Web"); //Object type String or new String --> Heap
		System.out.println(System.identityHashCode(str4));//3000
	}
}
