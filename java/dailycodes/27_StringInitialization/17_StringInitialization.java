class String17{

	public static void main(String[] args){
	
		// Type 1 : 
		
		String str1 = "Shashi";
		System.out.println(str1);

		// Type 2 :
		
		char arr[] = new char[]{'a','s','h','i','s','h'};
		System.out.println(arr);

		// Type 3

		String str2 = new String("Core2Web");
		System.out.println(str2);
	}
}
