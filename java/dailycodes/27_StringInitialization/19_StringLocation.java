class String19{

	public static void main(String[] args){
	
		String str1 = "Shashi";
		System.out.println(str1);// Merhod Area --> SCP
		System.out.println(System.identityHashCode(str1));// 1000

		String str2 = new String("Shashi");
		System.out.println(str2);// Heap Section
		System.out.println(System.identityHashCode(str2));// 2000
	}
}
