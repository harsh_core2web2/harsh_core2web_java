class String22{

	public static void main(String[] args){
	
		String str1= "Shashi"; 
		System.out.println(System.identityHashCode(str1));//1000

		String str2= "Shashi"; 
		System.out.println(System.identityHashCode(str2));//1000
		
		String str3 = new String("Shashi"); 
		System.out.println(System.identityHashCode(str3));//2000
		
		String str4 = new String("Shashi"); 
		System.out.println(System.identityHashCode(str4));//3000
	}
}
