class MyThread extends Thread{

	public void run(){
	
		System.out.println(Thread.currentThread().getName());//Thread - 0
		System.out.println(Thread.currentThread());//[Thread-0,5,main]
		System.out.println(getName());//Thread-0

	}
}

class Client{

	public static void main(String[] args){
	
		MyThread t1 = new MyThread();
		t1.start();

		System.out.println(Thread.currentThread().getName());// main
		System.out.println(Thread.currentThread());// [main,5,main]
	}
}
