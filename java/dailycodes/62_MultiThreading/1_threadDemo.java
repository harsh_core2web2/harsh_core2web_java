class ThreadDemo extends Thread{

	static {
	
		System.out.println(10/0);
	}
	public void run(){
	
		System.out.println(Thread.currentThread().getName());

		for(int i = 1; i <= 5; i++){
		
			System.out.println("In run");
		}
	}

	public static void main(String[] args)throws InterruptedException{
	
		System.out.println(Thread.currentThread().getName());
		ThreadDemo t1 = new ThreadDemo();
		t1.start();

		for(int i = 1; i <= 50 ; i++){
			System.out.println("Before Sleep");
		}

		Thread.sleep(1000);

		for(int i = 1; i <= 50 ; i++){
			System.out.println("In Main");
		}
	}
}
