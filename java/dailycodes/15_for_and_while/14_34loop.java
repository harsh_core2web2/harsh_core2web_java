class code34{//We change x with i ; No error because I is only valid for that loop after {} i is not valid i.e dead

	public static void main(String[] args){
	
		for(int i = 1 ; i<=5 ; i++){
		
			System.out.println("Hello Luffy "+i);
		}

		int i = 1;

		while(i<=5){
		
		
			System.out.println("Hello Luffy : " +i);
			i++;
		}
	}
}
