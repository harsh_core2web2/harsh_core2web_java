// Static varibles cannot be writtern in static method
class demo3{

	static void fun(){
	
		static x = 10;
	}

	public static void main(String[] args){
	
		int a = 30;

		static int b = 40;
	}
}
