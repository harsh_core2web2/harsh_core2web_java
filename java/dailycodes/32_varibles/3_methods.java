
class methodDemo3{

	void fun(){
	
		System.out.println("In fun");
	}

	static void run(){
	
		System.out.println("In run");
	}

	public static void main(String[] args){
	
		methodDemo3 obj = new methodDemo3();
		obj.fun();
		obj.run(); // static method can be accessed 1_Directly , 2_by creating object , 3_by class name
	}
}
