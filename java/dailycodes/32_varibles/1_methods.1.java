// Non static methods/varible cannot be accessed without creating an object

class methodDemo1{

	void fun(){
	
		System.out.println("In fun");
	}

	static void run(){
	
		System.out.println("In run");
	}

	public static void main(String[] args){
	
		fun();
	}
}
