class const2{

	const2(){
	
		super(); // It is used to call parent class

		return; // It is a method and to return from java stack return;  keyword is used 
			//	       	   (Note : no return value is given else it gives error)
	}

	void const2(){	// It is a method with return value void so compiler will not consider it as a constructor
	

	}
}
