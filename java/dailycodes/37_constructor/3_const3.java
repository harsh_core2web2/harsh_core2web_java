class const3{

	const3(){
		
		System.out.println("In Constructor");
	}

}

class constDemo{

	public static void main(String[] args){
	
		System.out.println("Start Main");
		const3 obj = new const3();		// Constructor is called after creating an object
		System.out.println("End Main");
	}
}
