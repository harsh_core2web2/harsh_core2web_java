class xyz{

	void gun(){
	
		System.out.println("In gun");
	}

	void fun(float x){
	
		System.out.println("In fun");
		System.out.println(x);

	}
}

class demo{

	void run(short i,char f){
		
		System.out.println("In Run");
		System.out.println(i);
		System.out.println(f);
	
	}
	
	public static void main(String[] args){
	
		xyz obj1 = new xyz();
		demo obj2 = new demo();

		obj1.fun(10.5f);
		obj2.run('A',32);
		obj1.fun(10);
		obj1.fun(50l);
		obj1.fun(20.5);
	}
}
