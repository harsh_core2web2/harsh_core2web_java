class ThreadDemo extends Thread{

	public void run(){
	
		System.out.println(getPriority());
		System.out.println(Thread.currentThread().getPriority());
	}
}

class Client{

	public static void main(String[] args){
	
		
		System.out.println(Thread.currentThread().getPriority());

		ThreadDemo td = new ThreadDemo();

		System.out.println(td.getPriority());

		td.setPriority(7);
		td.start();
		td.setPriority(11);


	}
}
