class io5{

	public static void main(String[] args){
	
		int num1 = "10"; // Incompatible types : String cannot be converted to int

		int num2 = "20"; // Incompatible types : String cannot be converted to int

		System.out.println(num1+num2);
	}
}
