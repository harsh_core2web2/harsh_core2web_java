import java.util.*;

class ListMethods{

	public static void main(String[] args){
	
		List al = new ArrayList();

		// add()
		al.add(10);
		al.add(20);
		al.add("Shashi");
		al.add("Kanha");
		al.add(30.5);

		System.out.println(al);
		
		//add(int,E)
		al.add(3,"Rahul");
		System.out.println(al);
		al.add(5,"Ashish");
		System.out.println(al);

		//contains() get empty remove isEmpty 
		System.out.println(al.contains(10));

		System.out.println(al.get(1));


	}
}
